package br.com.itau;

public class Carta {
    public static final int PAUS=1, OUROS=2, ESPADAS=3, COPAS=4;

    private int naipe;
    private int valor;

    public Carta(int n, int v){
        //naipe = n &gt= PAUS && n &lt= COPAS ? n : 1;
        //valor = v &gt= 1 && n &lt= 13 ? v : 1;
        naipe = n >= PAUS && n <= COPAS ? n : 1;
        valor = v >= 1 && n <= 13 ? v : 1;
    }

    public int getNaipe(){
        return naipe;
    }

    public int getValor(){
        return valor;
    }

    public String toString(){
        String tmp = null;
        switch(valor){
            case 1: tmp = "as"; break;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10: tmp = valor +""; break;
            case 11: tmp = "valete"; break;
            case 12: tmp = "dama"; break;
            case 13: tmp = "rei"; break;
        }
        switch(naipe){
            case PAUS: return tmp + " de paus";
            case OUROS: return tmp + " de ouros";
            case ESPADAS: return tmp + " de espadas";
            case COPAS: return tmp + " de copas";
        }
        return tmp;
    }
}