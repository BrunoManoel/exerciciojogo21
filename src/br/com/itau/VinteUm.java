package br.com.itau;

public class VinteUm {
    public static void main(String[] args) {
        int jogador = 0, pontosJogador = 0;
        int comput = 0, pontosComput = 0;

        Carta cartasJogador[] = new Carta[19];
        Carta cartasComput[] = new Carta[19];

        Baralho b = new Baralho();
        b.embaralhar();
        String resp = "N";
        do{
            cartasJogador[jogador++] = b.proximaCarta();
            System.out.println("\n Suas cartas: ");
            mostraCartas(cartasJogador);
            pontosJogador = somaCartas(cartasJogador);
            if(pontosJogador < 21){
                System.out.println("Quer carta? (S|N");
                resp = Input.readString();
            }
        }while(resp.equals("S") && pontosJogador < 21);
        if(pontosJogador > 21){
            System.out.println("Você perdeu!");
            return;
        }
        System.out.println("\n Minhas cartas:");
        while(pontosComput < pontosJogador && pontosComput != 21){
            cartasComput[comput++] = b.proximaCarta();
            pontosComput = somaCartas(cartasComput);
        }
        mostraCartas(cartasComput);
        if(pontosComput >= pontosJogador && pontosComput != 21){
            System.out.println("\n Você perdeu!");
        }else{
            System.out.println("\n Você ganhou!");
        }
    }

    private static int somaCartas(Carta mao[]){
        int i = 0,  pontos = 0;
        while(mao[i] != null){
            if(mao[i].getValor() > 10)
            pontos++;
			else
            pontos += mao[i].getValor();
            i++;
        }
        return pontos;
    }

    private static void mostraCartas(Carta mao[]){
        int i = 0;
        while(mao[i] != null){
            System.out.println((i != 0 ? " , "  : " ")+ mao[i].toString());
            i++;
        }
        System.out.println("\n Pontos = " + somaCartas(mao));
    }
}